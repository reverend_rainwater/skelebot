package main

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v3"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"skelebot/bones"
)

func init() {
	// Handle execution flags.
	flag.StringVar(&bones.Config.Config_File, "config", "skelebot.yml", "Config File")
	flag.Parse()
	// Some logging for which config file is being used.
	if bones.Config.Config_File == "skelebot.yml" {
		log.Println(bones.INFO + " Using adjacent config file: skelebot.yml")
	} else {
		log.Println(bones.INFO + " Using config file specified at: " + bones.Config.Config_File)
	}
	// Read config file.
	conf, err := os.ReadFile(bones.Config.Config_File)
	if err != nil {
		log.Fatal(err)
	}
	// Unmarshal config file to the config object.
	err2 := yaml.Unmarshal(conf, &bones.Config)
	if err2 != nil {
		log.Fatal(err2)
	}
	// Check to see if the config prefix is not empty nor a space.
	// If true, replace with provided prefix.
	if bones.Config.Prefix != "" && bones.Config.Prefix != " " {
		bones.PREFIX = bones.Config.Prefix
	}
	// Check to see if the Discord token was provided.
	// If none provided, then fatal log.
	if bones.Config.Token == "" {
		log.Fatal(bones.ERR + "Discord token must be provided!")
	}
	bones.InitializeBrain()
}

func main() {
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + bones.Config.Token)
	if err != nil {
		log.Fatal(bones.ERR+"Error creating Discord session,", err)
		return
	}
	// Register the guildMessage function as a callback for MessageCreate events.
	dg.AddHandler(guildMessage)
	// Register slash commands.
	dg.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := bones.CommandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})
	// Set intents on receiving guild message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages
	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		log.Fatal(bones.ERR+"Error opening connection,", err)
		return
	}
	// TODO: remove this shit ass way of handling sessions.
	bones.Session = dg
	// Register all the defined commands for skelebot.
	registeredCommands := make([]*discordgo.ApplicationCommand, len(bones.Commands))
	for i, v := range bones.Commands {
		cmd, err1 := dg.ApplicationCommandCreate(dg.State.User.ID, "", v)
		if err1 != nil {
			log.Panicf("Cannot create '%v' command: %v", v.Name, err)
		}
		registeredCommands[i] = cmd
	}
	// Print logo thing.
	fmt.Println(bones.SKELE)
	fmt.Println("\n=---------------- LOGGING START ----------------=")
	// Wait here until CTRL-C or other term signal is received.
	log.Println(bones.INFO + " Skelebot is now running. Press Ctrl-C to shutdown.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	bones.Logging(bones.Config, bones.Session, bones.INFO, "Shutting down Skelebot!")
	// Cycle through commands registered for skelebot and remove them.
	for _, v := range registeredCommands {
		err := dg.ApplicationCommandDelete(dg.State.User.ID, "", v.ID)
		if err != nil {
			log.Panicf("Cannot delete '%v' command: %v", v.Name, err)
		}
	}
	bones.BrainSave("skele.brain", bones.Brain)
	// Cleanly close down the Discord session.
	_ = dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func guildMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	// Learning!
	if bones.Config.Learning {
		bones.BrainLearn(m.Content)
	}
	if 42 == rand.Intn(1000) {
		// meme-factory
		_, _ = s.ChannelMessageSend("240196923644968960", bones.BrainGenerateResponse())
		// generul, shortstacks
		_, _ = s.ChannelMessageSend("867508184629510164", bones.BrainGenerateResponse())
	}
	// Pass bones.Session to global
	// TODO: remove this, dumbass
	bones.Session = s
	bones.Guild, _ = s.Guild(m.GuildID)
}
