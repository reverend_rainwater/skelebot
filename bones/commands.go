package bones

import (
	"github.com/bwmarrin/discordgo"
	"log"
)

var (
	Commands = []*discordgo.ApplicationCommand{
		{
			Name:        "ping",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Check to see if the bot is up.",
		},
		{
			Name:        "status",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Check the server's status.",
		},
		{
			Name:        "source",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get a link to Skelebot's source code.",
		},
		{
			Name:        "help",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get a list of available commands.",
		},
		{
			Name:        "team",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get a description of a team member.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "name",
					Description: "Name of the person you would like to know more about.",
					Required:    true,
				},
			},
		},
		{
			Name:        "youtube",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search YouTube.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "What you want to search YouTube for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "image",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search Google Images.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "What you want to search Google Images for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "google",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search Google.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "What you want to search Google for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "weather",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get the weather for a location.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "location",
					Description: "Location, works best with city names.",
					Required:    true,
				},
			},
		},
		{
			Name:        "answer",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search for an answer.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "What you want to know.",
					Required:    true,
				},
			},
		},
		{
			Name:        "math",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Solve a given math problem.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "Math problem you'd like to solve.",
					Required:    true,
				},
			},
		},
		{
			Name:        "myanimelist",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search MyAnimeList for an anime.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "title",
					Description: "What you want to search MyAnimeList for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "anilist",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Search Anilist for an anime.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "title",
					Description: "What you want to search Anilist for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "touhou",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get a random Touhou image.",
		},
		{
			Name:        "eightball",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Shake an eightball, get a response.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "question",
					Description: "Max number of the range you want to pick between.",
					Required:    true,
				},
			},
		},
		{
			Name:        "coin",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Flip a coin.",
		},
		{
			Name:        "pick",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Pick a number within a specified range.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "range",
					Description: "Max number of the range you want to pick between.",
					Required:    true,
				},
			},
		},
		{
			Name:        "choose",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Choose something from a comma separated list.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "list",
					Description: "List things, separated by commas.",
					Required:    true,
				},
			},
		},
		{
			Name:        "dice",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Roll dice.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "dice",
					Description: "Use the format 'xdy' where x is the number of dice and y is the number of sides.",
					Required:    true,
				},
			},
		},
		{
			Name:        "qi",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Send random quote image.",
		},
		// Cryptography, security, etc.
		{
			Name:        "morse",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Encrypt to morse.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "message",
					Description: "Message to encrypt to morse.",
					Required:    true,
				},
			},
		},
		{
			Name:        "demorse",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Decrypt from morse.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "message",
					Description: "Message to decrypt from morse.",
					Required:    true,
				},
			},
		},
		{
			Name:        "checklink",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Check to see if a link is malicious.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "link",
					Description: "Link (URL) you would like to check.",
					Required:    true,
				},
			},
		},
		// AI and shit
		{
			Name:        "gpt-3",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Use GPT-3 to generate text.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "input",
					Description: "Input for generating the text.",
					Required:    true,
				},
			},
		},
		{
			Name:        "greentext",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get a greentext about the provided topic.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "topic",
					Description: "Topic you want the greentext about.",
					Required:    true,
				},
			},
		},
		{
			Name:        "text2image",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get an image from a text prompt.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "prompt",
					Description: "Prompt for the image generation.",
					Required:    true,
				},
			},
		},
		{
			Name:        "text2image-pro",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get an image from a text prompt. Pro version.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "prompt",
					Description: "Prompt for the image generation.",
					Required:    true,
				},
			},
		},
		{
			Name:        "markov",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Generate a text response based off the server's conversations.",
		},
		{
			Name:        "markotext",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get an greentext from a Markov generated prompt.",
		},
		{
			Name:        "markoimage",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Get an image from a Markov generated prompt.",
		},
		{
			Name:        "togglelearning",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Toggle Skelebot's ability to learn on or off.",
		},
		{
			Name:        "savebrain",
			Type:        discordgo.ChatApplicationCommand,
			Description: "Save Skelebot's current brain to a local brain file.",
		},
	}

	CommandHandlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		// Utility
		"ping": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			Guild, _ := s.Guild(i.GuildID)
			Usage(Config, INFO, i.Interaction.Member.Nick, Guild.Name, "Responding to a ping!")
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Fuck you, I'm awake.",
				},
			})
			if err != nil {
				log.Println(err)
			}
		},
		"status": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbed(s, i, Status, STATUS)
		},
		"source": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, GetSourceCode, COM)
		},
		"help": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, GetCommandList, HELP)
		},
		"team": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, GetTeamMember, TEAM, "name")
		},

		// Google, YouTube, Images, etc.
		"youtube": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, YoutubeSearch, YOUTUBE, "query")
		},
		"youtubeproxy": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseFileOptions(s, i, YouTubeProxy, YOUTUBE, "query")
		},
		"image": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, ImageSearch, GOOGLE, "query")
		},
		"google": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, GoogleSearch, GOOGLE, "query")
		},
		"weather": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, GetWeather, WEATHER, "location")
		},
		"answer": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, Skelesearch, GOOGLE, "query")
		},
		"math": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, MathSolve, WOLF, "query")
		},
		// Anime
		"myanimelist": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, MALSearch, ANIME, "title")
		},
		"anilist": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, AnilistSearch, ANIME, "title")
		},
		"touhou": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, GetTouhou, TOUHOU)
		},
		// Games
		"eightball": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, EightBall, EIGHTBALL, "question")
		},
		"coin": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, Coin, COIN)
		},
		"pick": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, Pick, PICK, "range")
		},
		"choose": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, Choose, CHOOSE, "list")
		},
		"dice": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, Dice, DICE, "dice")
		},
		"qi": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			QuoteImage(s, i)
		},
		// Security, cryptography, etc.
		"morse": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, Morse, MORSE, "message")
		},
		"demorse": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseTextOptions(s, i, Demorse, MORSE, "message")
		},
		"checklink": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, CheckURL, SEC, "link")
		},
		// AI
		"gpt-3": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, GPT3, AI, "input")
		},
		"greentext": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, GetGreentextEmbed, AI, "topic")
		},
		"text2image": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, TextToImage, AI, "prompt")
		},
		"text2image-pro": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedOptions(s, i, DALLEImage, AI, "prompt")
		},
		"markov": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, BrainGenerateResponse, MARKOV)
		},
		"markotext": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedPrompt(s, i, GetGreentextEmbed, MARKOV, BrainGenerateResponse())
		},
		"markoimage": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseEmbedPrompt(s, i, TextToImage, MARKOV, BrainGenerateResponse())
		},
		"togglelearning": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			InteractionResponseText(s, i, ToggleLearning, BRAIN)
		},
		"savebrain": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			Usage(Config, BRAIN, i.Interaction.Member.Nick, Guild.Name, "Manually saving brain.")
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponsePong,
				Data: &discordgo.InteractionResponseData{
					Content: "Saving current brain to local brain file.",
				},
			})
			if err != nil {
				log.Println(err)
			}
			BrainSave("skele.brain", Brain)
		},
	}
)
