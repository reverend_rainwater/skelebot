/*
Bones :: Security:

Security contains functions related to security.
This meaning things information security related,
encryption, etc.
*/
package bones

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/alwindoss/morse"
	"github.com/bwmarrin/discordgo"
	"github.com/imroc/req"
)

type Engine struct {
	Name      string
	Reference string
	Detected  bool
}

type Blacklist struct {
	Engines    []Engine `json:"engines"`
	Detections int      `json:"detections"`
}

type Check struct {
	IsAccessible           bool `json:"is_url_accessible"`
	IsSuspiciousURLPattern bool `json:"is_suspicious_url_pattern"`
	IsPotentialPhishing    bool `json:"is_phishing_heuristic"`
	IsMostAbusedTLD        bool `json:"is_most_abused_tld"`
	IsBlacklisted          bool `json:"is_domain_blacklisted"`
	IsSuspicious           bool `json:"is_suspicious_domain"`
	IsRisky                bool `json:"is_risky_category"`
	IsRecentlyMade         bool `json:"is_domain_recent"`
	HasInvalidHTTPS        bool `json:"is_valid_https"`
	IsChinese              bool `json:"is_china_country"`
	IsNigerian             bool `json:"is_nigeria_country"`
}

type Server struct {
	IP      string `json:"ip"`
	Country string `json:"country_name"`
	Region  string `json:"region_name"`
	City    string `json:"city_name"`
	ISP     string `json:"isp"`
	ASN     string `json:"asn"`
}

type Category struct {
	IsTorrent         bool `json:"is_torrent"`
	IsVPN             bool `json:"is_vpn_provider"`
	IsFreeHosting     bool `json:"is_free_hosting"`
	IsAnonymizer      bool `json:"is_anonymizer"`
	IsURLShortener    bool `json:"is_url_shortener"`
	IsFreeDynamicDNS  bool `json:"is_free_dynamic_dns"`
	IsCodeSandbox     bool `json:"is_code_sandbox"`
	IsFormBuidler     bool `json:"is_form_builder"`
	IsFreeFileSharing bool `json:"is_free_file_sharing"`
	IsPastebin        bool `json:"is_pastebin"`
}

type APIvoid struct {
	Data struct {
		Report struct {
			Blacklists Blacklist `json:"domain_blacklist"`
			RiskScore  struct {
				Risk int `json:"result"`
			} `json:"risk_score"`
			Checks     Check    `json:"security_checks"`
			ServerInfo Server   `json:"server_details"`
			Categories Category `json:"site_categories"`
			URL        struct {
				Host string `json:"host"`
			} `json:"url_parts"`
		} `json:"report"`
	} `json:"data"`
}

// Demorse - decode from morse.
func Demorse(input string) string {
	Logging(Config, Session, MORSE, "Input to decode: "+input)
	// Create morse hacker.
	hacker := morse.NewHacker()
	// Decode the morse message.
	demorse, err := hacker.Decode(strings.NewReader(input))
	if err != nil {
		Logging(Config, Session, WARN, "Issue decoding morse string.")
		return "Issue decoding."
	}
	Logging(Config, Session, MORSE, "Successfully decoded.")
	return string(demorse)
}

// Morse - encode to morse.
func Morse(input string) string {
	Logging(Config, Session, MORSE, "Input to encode: "+input)
	// Create morse hacker.
	hacker := morse.NewHacker()
	// Encode the message to morse.
	morse, err := hacker.Encode(strings.NewReader(input))
	if err != nil {
		Logging(Config, Session, WARN, "Issue encoding string to morse.")
		return "Issue encoding string to morse."
	}
	Logging(Config, Session, MORSE, "Successfully encoded input to morse.")
	return string(morse)
}

// CheckURL - check a URL agains URLvoid.
func CheckURL(input string) discordgo.MessageEmbed {
	Logging(Config, Session, SEC, "Checking URL against URLvoid: `"+input+"`")
	// Make thumbnails
	var CleanThumbnail discordgo.MessageEmbedThumbnail
	CleanThumbnail.URL = "https://cdn.discordapp.com/emojis/837865945065586699.gif"
	var SuspiciousThumbnail discordgo.MessageEmbedThumbnail
	SuspiciousThumbnail.URL = "https://cdn.discordapp.com/emojis/581596284444737546.gif"
	var MaliciousThumbnail discordgo.MessageEmbedThumbnail
	MaliciousThumbnail.URL = "https://cdn.discordapp.com/emojis/837865929328689153.gif"
	var message discordgo.MessageEmbed
	// Generate request client using these headers and params.
	header := req.Header{
		"Accept": "application/json",
	}
	param := req.Param{
		"key": Config.APIvoid,
		"url": url.QueryEscape(input),
	}
	Logging(Config, Session, SEC, "Generating URLvoid request client.")
	// Make request.
	r, err := req.Get(APIVOID_URL, header, param)
	if err != nil {
		Logging(Config, Session, WARN, "Error generating URLvoid request client.")
		return message
	}
	var response APIvoid
	Logging(Config, Session, SEC, "Parsing URLvoid JSON response.")
	// Parse response from JSON.
	r.ToJSON(&response)
	Logging(Config, Session, SEC, "Generating embedded message from URLvoid response.")
	// Generate embedded message.
	var footer discordgo.MessageEmbedFooter
	footer.Text = "Made with <3 by Dr. Skelebones of Skele's Choice"
	message.Footer = &footer
	server := response.Data.Report.ServerInfo
	checks := response.Data.Report.Checks
	categories := response.Data.Report.Categories
	blacklists := response.Data.Report.Blacklists
	riskscore := response.Data.Report.RiskScore
	host := response.Data.Report.URL.Host
	if IsMalicious(response.Data.Report.Checks, riskscore.Risk) {
		message.Title = "URL IS MALICIOUS!"
		message.Color = 16711680
		message.Thumbnail = &MaliciousThumbnail
	} else if IsSuspicious(response.Data.Report.Checks, riskscore.Risk) {
		message.Title = "URL is suspicious!"
		message.Color = 16777011
		message.Thumbnail = &SuspiciousThumbnail
	} else {
		message.Title = "URL is clean!"
		message.Color = 3394662
		message.Thumbnail = &CleanThumbnail
	}
	message.Description = "```" + GetSummary(riskscore.Risk, host, server, checks, categories, blacklists) + "```"
	return message
}

// GetSummary - generates the summary message from the response.
// TODO: Anything else but this. Fix it.
func GetSummary(score int, host string, server Server, checks Check, categories Category, blacklists Blacklist) string {
	Logging(Config, Session, SEC, "Generating URLvoid message summary.")
	var summary string
	summary = summary + "---- Basic Information ----\n"
	summary = summary + "Domain: " + host + "\n"
	summary = summary + "IP: " + server.IP + "\n"
	summary = summary + "ASN: " + server.ASN + "\n"
	summary = summary + "Country: " + server.Country + "\n"
	summary = summary + "Region: " + server.Region + "\n"
	summary = summary + "City: " + server.City + "\n"
	summary = summary + "---- Risk Assessment ----\n"
	summary = summary + fmt.Sprintf("Risk Score: %d/100\n", score)
	summary = summary + fmt.Sprintf("Detections: %d\n", blacklists.Detections)
	summary = summary + "---- Checks & Categories ----\n"
	if checks.IsAccessible {
		summary = summary + INFO + "The URL is accessible.\n"
	}
	if checks.IsSuspiciousURLPattern {
		summary = summary + WARN + "URL pattern is suspicious!\n"
	}
	if checks.IsPotentialPhishing {
		summary = summary + WARN + "URL is potentially used for phishing!\n"
	}
	if checks.IsBlacklisted {
		summary = summary + WARN + "URL is on at least one blacklist!\n"
	}
	if checks.IsMostAbusedTLD {
		summary = summary + WARN + "URL uses a TLD which is used for abuse!\n"
	}
	if checks.IsSuspicious {
		summary = summary + WARN + "URL is suspicious!\n"
	}
	if checks.IsRisky {
		summary = summary + WARN + "URL is risky!\n"
	}
	if checks.IsRecentlyMade {
		summary = summary + WARN + "URL is recently made!\n"
	}
	if !checks.HasInvalidHTTPS {
		summary = summary + WARN + "URL does not have valid HTTPS!\n"
	}
	if checks.IsChinese {
		summary = summary + WARN + "URL is hosted or owned by a Chinese company!\n"
	}
	if checks.IsNigerian {
		summary = summary + WARN + "URL is hosted or owned by a Nigerian company!\n"
	}
	if categories.IsTorrent {
		summary = summary + WARN + "The URL is a torrent site or is associated with torrenting.\n"
	}
	if categories.IsVPN {
		summary = summary + INFO + "The URL associated or owned by a VPN company..\n"
	}
	if categories.IsFreeHosting {
		summary = summary + INFO + "The URL is hosted on a free hosting provider.\n"
	}
	if categories.IsAnonymizer {
		summary = summary + INFO + "The URL is an anonymizer.\n"
	}
	if categories.IsURLShortener {
		summary = summary + INFO + "The URL is a URL shortner.\n"
	}
	if categories.IsFreeDynamicDNS {
		summary = summary + INFO + "The URL is a free dynamic DNS provider.\n"
	}
	if categories.IsCodeSandbox {
		summary = summary + INFO + "The URL is a code sandbox website.\n"
	}
	if categories.IsFormBuidler {
		summary = summary + INFO + "The URL is a form builder.\n"
	}
	if categories.IsFreeFileSharing {
		summary = summary + WARN + "The URL is a file sharing service!.\n"
	}
	if categories.IsPastebin {
		summary = summary + INFO + "The URL is for Pastebin.\n"
	}
	return summary
}

// IsMalicious - returns if URL is malicious.
func IsMalicious(checks Check, risk int) bool {
	Logging(Config, Session, SEC, "Checking if URL is malicious.")
	if risk > 69 {
		return true
	}
	return (checks.IsBlacklisted || checks.IsPotentialPhishing || checks.IsMostAbusedTLD)
}

// IsSuspicious - returns if URL is suspicious.
func IsSuspicious(checks Check, risk int) bool {
	Logging(Config, Session, SEC, "Checking if URL is suspicious.")
	if risk > 40 && risk < 70 {
		return true
	}
	return (checks.IsSuspicious || checks.IsSuspiciousURLPattern || checks.IsRecentlyMade || checks.IsChinese || checks.IsNigerian || !checks.HasInvalidHTTPS)
}
