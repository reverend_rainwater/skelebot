package bones

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func GetSlashCommandOptionMap(data discordgo.ApplicationCommandInteractionData) map[string]*discordgo.ApplicationCommandInteractionDataOption {
	optionsRaw := data.Options
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(optionsRaw))
	for _, opt := range optionsRaw {
		optionMap[opt.Name] = opt
	}

	return optionMap
}

func InteractionResponseText(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionText, interactionType string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, "")
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Content: function(),
	})
	if err != nil {
		log.Println(err)
	}
}

func InteractionResponseTextOptions(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionTextQuery, interactionType string, optionKey string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	optionMap := GetSlashCommandOptionMap(i.ApplicationCommandData())
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, optionMap[optionKey].StringValue())
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Content: function(optionMap[optionKey].StringValue()),
	})
	if err != nil {
		log.Println(err)
	}
}

func InteractionResponseFileOptions(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionFileQuery, interactionType string, optionKey string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	optionMap := GetSlashCommandOptionMap(i.ApplicationCommandData())
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, optionMap[optionKey].StringValue())
	temp := function(optionMap[optionKey].StringValue())
	file := discordgo.File{Reader: &temp}
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Content: "It's still broke, fucking idiot poopy head.",
		Files:   []*discordgo.File{&file},
	})
	if err != nil {
		log.Println(err)
	}
}

func InteractionResponseEmbed(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionEmbed, interactionType string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, "")
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	message := function()
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Embeds: []*discordgo.MessageEmbed{&message},
	})
	if err != nil {
		log.Println(err)
	}
}

func InteractionResponseEmbedPrompt(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionEmbedQuery, interactionType string, prompt string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, prompt)
	message := function(prompt)
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Embeds: []*discordgo.MessageEmbed{&message},
	})
	if err != nil {
		log.Println(err)
	}
}

func InteractionResponseEmbedOptions(s *discordgo.Session, i *discordgo.InteractionCreate, function InteractionEmbedQuery, interactionType string, optionKey string) {
	interactionGuild, _ := s.Guild(i.GuildID)
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	optionMap := GetSlashCommandOptionMap(i.ApplicationCommandData())
	Usage(Config, interactionType, i.Interaction.Member.Nick, interactionGuild.Name, optionMap[optionKey].StringValue())
	message := function(optionMap[optionKey].StringValue())
	_, err = s.FollowupMessageCreate(i.Interaction, true, &discordgo.WebhookParams{
		Embeds: []*discordgo.MessageEmbed{&message},
	})
	if err != nil {
		log.Println(err)
	}
}

// IsNotModerator - checks to see if the member is a moderator.
func IsNotModerator(member *discordgo.Member) bool {
	for _, roleID := range member.Roles {
		for _, modRole := range Config.AllowedRoles {
			if roleID == modRole {
				return false
			}
		}
	}
	return true
}

// GetCommand - gets the command of a message.
func GetCommand(message string) string {
	parts := strings.Split(message, " ")
	return strings.TrimPrefix(parts[0], PREFIX)
}

// RemoveCommand - removes the command from a message.
func RemoveCommand(m string, c string) string {
	return strings.TrimSpace(strings.TrimPrefix(m, PREFIX+c))
}

func Usage(config Configuration, eventtype string, user string, guild string, message string) {
	url := "https://" + config.HumioURL + "/api/v1/ingest/humio-unstructured"
	method := "POST"
	payload := strings.NewReader(`[
		{
			"fields": {
				"type": "` + eventtype + `",
				"user": "` + user + `",
				"guild": "` + guild + `"
			}, 
			"messages": [
				"` + message + `"
			]
		}
	]`)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("Authorization", "Bearer "+config.HumioUsageToken)
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()
}

func Logging(config Configuration, session *discordgo.Session, eventtype string, message string) {
	url := "https://" + config.HumioURL + "/api/v1/ingest/humio-unstructured"
	method := "POST"
	payload := strings.NewReader(`[
		{
			"fields": {
				"type": "` + eventtype + `"
			}, 
			"messages": [
				"` + message + `"
			]
		}
	]`)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("Authorization", "Bearer "+config.HumioLoggingToken)
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()
	log.Println("[" + eventtype + "] " + message)
	session.ChannelMessageSend(Config.Logging, "["+eventtype+"] "+message)
}
