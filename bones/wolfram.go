package bones

import (
	"github.com/bwmarrin/discordgo"
	"strings"

	"github.com/imroc/req"
)

type Subpod struct {
	Title  string `json:"title"`
	Result string `json:"plaintext"`
}

type Pod struct {
	Title   string   `json:"title"`
	Subpods []Subpod `json:"subpods"`
}

type QueryResults struct {
	Successful bool  `json:"success"`
	Error      bool  `json:"error"`
	Pods       []Pod `json:"pods"`
}

type Search struct {
	Results QueryResults `json:"queryresult"`
}

const WOLFRAM = "https://api.wolframalpha.com/v2/query"

func Skelesearch(input string) discordgo.MessageEmbed {
	Logging(Config, Session, SEARCH, "Searching for `"+input+"`")
	// Generate request client using these headers and params.
	header := req.Header{
		"Accept": "application/json",
	}
	param := req.Param{
		"input":  input,
		"format": "plaintext",
		"output": "JSON",
		"appid":  Config.Wolfram,
	}
	Logging(Config, Session, WOLF, "Generating Wolfram request client.")
	message := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Error generating request client.",
		},
	}
	// Make request.
	r, err := req.Get(WOLFRAM, header, param)
	if err != nil {
		Logging(Config, Session, WARN, "Error generating Wolfram request client.")
		return message
	}
	var response Search
	Logging(Config, Session, WOLF, "Parsing Wolfram JSON response.")
	// Parse response from JSON.
	err = r.ToJSON(&response)
	if err != nil {
		message.Footer.Text = "Error parsing JSON response."
		return message
	}

	for _, result := range response.Results.Pods {
		if result.Title == "Result" && result.Subpods[0].Result != "(data not available)" {
			message.Description = result.Subpods[0].Result
			message.Footer.Text = "Searched for: " + strings.TrimSpace(input)
			return message
		}
		if result.Title == "Exact result" {
			if strings.Contains(result.Subpods[0].Result, "(irreducible)") {
				message.Description = response.Results.Pods[2].Subpods[0].Result
				message.Footer.Text = "Searched for: " + strings.TrimSpace(input)
				return message
			}
			message.Description = result.Subpods[0].Result
			message.Footer.Text = "Searched for: " + strings.TrimSpace(input)
			return message
		}
	}
	// Default to Google.
	result := GoogleSearch(input)
	return result
}

func MathSolve(input string) discordgo.MessageEmbed {
	Logging(Config, Session, WOLF, "Solving math for `"+input+"`")
	// Generate request client using these headers and params.
	header := req.Header{
		"Accept": "application/json",
	}
	param := req.Param{
		"input":    input,
		"podstate": "Result__Step-by-step solution",
		"format":   "plaintext",
		"output":   "JSON",
		"appid":    Config.Wolfram,
	}
	Logging(Config, Session, WOLF, "Generating Wolfram request client.")
	message := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Error generating request client.",
		},
	}
	// Make request.
	r, err := req.Get(WOLFRAM, header, param)
	if err != nil {
		Logging(Config, Session, WARN, "Error generating Wolfram request client.")
		return message
	}
	var response Search
	Logging(Config, Session, WOLF, "Parsing Wolfram JSON response.")
	// Parse response from JSON.
	err = r.ToJSON(&response)
	if err != nil {
		message.Footer.Text = "Error parsing JSON response."
		return message
	}
	// Look for solution the return it if found.
	for _, result := range response.Results.Pods {
		if result.Title == "Results" || result.Title == "Exact result" {
			message.Description = "```perl\n" + result.Subpods[1].Result + "\n```"
			message.Footer.Text = "Solved for: " + strings.TrimSpace(input)
			return message
		}
	}
	// Default to Google haha
	result := GoogleSearch(input)
	return result
}
