<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://cdn.skele.dev/rowdy-required-oval-bumpy-pupa">
    <img src="https://cdn.skele.dev/rowdy-required-oval-bumpy-pupa/direct" alt="Logo">
  </a>

  <h3 align="center">Skelebot</h3>

  <p align="center">
    Skelebot by Skele's Choice is the #1 Discord bot for Skele's Choice employees to use!
	<br />
    Skele's Choice, it's the best choice! Ask your bones!
    <br />
    <br />
    <a href="https://skele.company">skele.company</a>
    ·
    <a href="https://gitlab.com/drskelebones/skelebot/-/issues">Report Unexpected Emergent Features</a>
    ·
    <a href="https://gitlab.com/drskelebones/skelebot/-/issues">Suggestions</a>
  </p>
</div>


## Configuration
Create a YAML file named "skelebot.yml" with the necessary information. An example is provided. 

## Commands
### Utility
- ping | pong - ping the bot.
- src | source - provide link to the git repo. 
- todo | task [todo request] - submits a Todoist request (currently only for developer unless you change the project ID in main).
- yt | youtube [query] - search YouTube for a video and return first result.
- img | image [query] - search Google Images and return first image.
- g | google [query] - search Google and return first result.
### Chance & Decision Making
- 8ball [question] - ask the magical 8ball something.
- flip | coin - flips an Ultrakill coin.
- pick [range] - picks a number within the provided range range.
- choose | decide [choice,choice,choice] - picks an item in the range given.
- aeburp | explode | aeurp - see if you exploded. 
- roll | dice [(1-20)d#] - roll 1 to 20 dice of a specified side count. 
- cringe [query] - calculate cringe percentage. 
### Anime
- mal | anime [query] - search MyAnimeList for anime. 
- anilist [query] - search Anilist for anime.
- morse [input] - encode to morse. 
- demorse [input] - decode from morse.
### Weather
- w | weather [location] - gets weather for a location.
### Security
- urlcheck | checkurl [URL] - checks a URL against URLvoid.
- pcheck [URL] - check a URL against Phisherman.
- preport [URL]- report a URL/domain to Phisherman.
